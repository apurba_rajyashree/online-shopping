FROM openjdk:17-jdk-alpine
ARG JAR_FILE=./target/*.jar
COPY ${JAR_FILE} online-shopping.jar
ENTRYPOINT ["java","-jar","/online-shopping.jar"]