package com.meta.onlineshopping.controller;

import com.meta.onlineshopping.dto.global.GlobalApiResponse;
import com.meta.onlineshopping.dto.user.UserRequestDto;
import com.meta.onlineshopping.service.user.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/save")
    public ResponseEntity<GlobalApiResponse> createUser(@Valid @RequestBody UserRequestDto userRequestDto) {
        userService.create(userRequestDto);
        return new ResponseEntity<>(new GlobalApiResponse("User created successfully", true, null), HttpStatus.OK);

    }


    @PutMapping("/update")
    public ResponseEntity<GlobalApiResponse> updateUser(@Valid @RequestBody UserRequestDto userRequestDto) {
        userService.update(userRequestDto);
        return new ResponseEntity<>(new GlobalApiResponse("User updated successfully", true, null), HttpStatus.OK);

    }

    @GetMapping("/find-by-id/{userId}")
    public ResponseEntity<GlobalApiResponse> getById(@PathVariable("userId") Integer userId) {
        return new ResponseEntity<>(new GlobalApiResponse("User fetched successfully", true, userService.findById(userId)), HttpStatus.OK);

    }

    @GetMapping
    public ResponseEntity<GlobalApiResponse> getAll() {
        return new ResponseEntity<>(new GlobalApiResponse("User list fetched successfully", true, userService.findAll()), HttpStatus.OK);

    }

    @DeleteMapping
    public ResponseEntity<GlobalApiResponse> deleteById(@RequestParam(value = "id") Integer id) {
        userService.deleteById(id);
        return new ResponseEntity<>(new GlobalApiResponse("User fetched successfully", true, null), HttpStatus.OK);

    }
}
