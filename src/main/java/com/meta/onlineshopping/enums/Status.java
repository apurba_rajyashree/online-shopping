package com.meta.onlineshopping.enums;

import lombok.Getter;

@Getter
public enum Status {

    PAYMENT_PENDING, PAYMENT_COMPLETED, PROCESSING, READY_TO_SHIP, SHIPPING, DELIVERED
}
