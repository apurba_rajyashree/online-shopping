package com.meta.onlineshopping.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "address")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "country", nullable = false, length = 150)
    private String country;

    @Column(name = "state", nullable = false)
    private String state;

    @Column(name = "street", nullable = false)
    private String street;

    @Column(name = "house_no", nullable = false, length = 10)
    private String houseNo;

    @Column(name = "zip_code", nullable = false, length = 20)
    private String zipCode;

    @OneToOne(fetch = FetchType.LAZY, targetEntity = User.class)
    @JoinColumn(name = "user_id", nullable = false, foreignKey = @ForeignKey(name = "fk_address_user"))
    private User user;

}
