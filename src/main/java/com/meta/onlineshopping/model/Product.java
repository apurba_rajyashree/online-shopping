package com.meta.onlineshopping.model;

import com.meta.onlineshopping.dto.product.ProductRequestDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "product")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "product_name", nullable = false)
    private String productName;

    @Column(name = "description", nullable = false, length = 1000)
    private String description;

    @Column(name = "price", nullable = false)
    private Double price;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    /**
     * cascade:
     * 1. CascadeType.ALL
     * 2. CascadeType.PERSIST
     * 2. CascadeType.MERGE
     * 4. CascadeType.REMOVE
     * 5. CascadeType.REFRESH
     * 6. CascadeType.DETACH
     */
    /**
     * fetch:
     * 1. FetchType.LAZY   --@ManyToMany & @OneToMany
     * 2. FetchType.EAGER  --@ManyTyOne & @OneToOne
     */
    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, foreignKey = @ForeignKey(name = "fk_product_user"))
    private User user;

    @ManyToMany(targetEntity = Category.class)
    @JoinTable(
            name = "product_category",
            joinColumns = @JoinColumn(name = "product_id", foreignKey = @ForeignKey(name = "fk_productcategory_product")),
            inverseJoinColumns = @JoinColumn(name = "category_id", foreignKey = @ForeignKey(name = "fk_productcategory_category"))
    )
    private List<Category> categoryList;


    public Product(ProductRequestDto productRequestDto) {
        this.id = productRequestDto.getId();
        this.productName = productRequestDto.getProductName();
        this.description = productRequestDto.getDescription();
        this.price = productRequestDto.getPrice();
        this.quantity = productRequestDto.getQuantity();
    }
}
