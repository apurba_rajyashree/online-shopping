package com.meta.onlineshopping.service.user;

import com.meta.onlineshopping.exceptions.DataNotFoundException;
import com.meta.onlineshopping.model.User;
import com.meta.onlineshopping.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByEmail(username).orElseThrow(
                () -> new DataNotFoundException("User is not present")
        );
        return user;
    }
}
