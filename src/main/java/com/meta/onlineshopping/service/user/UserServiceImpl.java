package com.meta.onlineshopping.service.user;

import com.meta.onlineshopping.dto.user.UserListResponseDto;
import com.meta.onlineshopping.dto.user.UserRequestDto;
import com.meta.onlineshopping.dto.user.UserResponseDto;
import com.meta.onlineshopping.enums.Role;
import com.meta.onlineshopping.exceptions.DataNotFoundException;
import com.meta.onlineshopping.model.User;
import com.meta.onlineshopping.repository.UserRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {


    @Autowired
    private PasswordEncoder encoder;

    private final UserRepo userRepo;


    @Override
    public void create(UserRequestDto userRequestDto) {
        User user = new User();
        user.setUsername(userRequestDto.getUsername());
        user.setFullName(userRequestDto.getUserFullName());
        user.setEmail(userRequestDto.getEmail());
        user.setMobileNumber(userRequestDto.getUserMobileNumber());
        user.setPassword(encoder.encode(userRequestDto.getPassword()));
        user.setGender(userRequestDto.getGender());
        user.setRole(Role.BUYER);
        user.setActive(true);

        userRepo.save(user);
    }

    @Override
    public void update(UserRequestDto userRequestDto) {
        Optional<User> optionalUser = userRepo.findById(userRequestDto.getId());
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setUsername(userRequestDto.getUsername());
            user.setUsername(userRequestDto.getUsername());
            user.setFullName(userRequestDto.getUserFullName());
            user.setEmail(userRequestDto.getEmail());
            user.setMobileNumber(userRequestDto.getUserMobileNumber());
            user.setPassword(encoder.encode(userRequestDto.getPassword()));
            user.setGender(userRequestDto.getGender());

            userRepo.save(user);
        } else {
            throw new DataNotFoundException("User with id " + userRequestDto.getId() + " is not present !!");
        }

    }

    @Override
    public UserResponseDto findById(Integer userId) {
        User user = userRepo.findById(userId).orElseThrow(
                () -> new DataNotFoundException("User with id " + userId + " is not present !!")
        );

        UserResponseDto userResponseDto = new UserResponseDto();
        userResponseDto.setUserFullName(user.getFullName());
        userResponseDto.setUserMobileNumber(user.getMobileNumber());
        userResponseDto.setEmail(user.getEmail());
        userResponseDto.setGender(user.getGender());
        userResponseDto.setRole(user.getRole());
        userResponseDto.setId(user.getId());
        userResponseDto.setActive(user.isActive());
        userResponseDto.setUsername(user.getUsername());

        return userResponseDto;
    }

    @Override
    public List<UserListResponseDto> findAll() {
        List<User> userList = userRepo.findAll();
        List<UserListResponseDto> userListResponseDtos = new ArrayList<>();
        for (User eachUser : userList) {
            UserListResponseDto userListResponseDto = new UserListResponseDto();
            userListResponseDto.setId(eachUser.getId());
            userListResponseDto.setUserFullName(eachUser.getFullName());
            userListResponseDto.setEmail(eachUser.getEmail());
            userListResponseDto.setUserMobileNumber(eachUser.getMobileNumber());
            userListResponseDto.setGender(eachUser.getGender());
            userListResponseDto.setRole(eachUser.getRole());

            userListResponseDtos.add(userListResponseDto);
        }
        return userListResponseDtos;
    }

    @Override
    public void deleteById(Integer userId) {
        User user = userRepo.findById(userId).orElseThrow(
                () -> new DataNotFoundException("User with id " + userId + " is not present !!")
        );
        user.setActive(false);  //soft delete
        userRepo.save(user);
    }
}
